# jwilder/nginx-proxy with CI/CD
[I use this VPS hosting](https://www.hostens.com/?affid=1251)

[jwilder nginx-proxy](https://github.com/jwilder/nginx-proxy/) allows you easily proxy any service and automatically get Let's Encrypt certificate for it.
#### Prerequrements
0. Optional. In case you want use GitLab CI/CD to deploy this project. You should add this variables to your CI/CD settings:
   
   | Variable | Description |
   | --- | --- |
   |`SERVER_KEY`| private ssh key. Add the public key to your server|
   |`SERVER_PORT`| ssh port|
   |`SERVER_USER`| ssh user|
   |`SERVER_HOST`| IP or domain of your server|

1. `docker` and `docker-compose` should be installed on your server

1. Make sure `docker` can be executed [without sudo](https://docs.docker.com/install/linux/linux-postinstall/)

1. Make sure domains you a trying to use a pointed to your server
    
    I use this DNS A recored `*.mydomain.com`

    This way I will have a lot of sites/project in one domain.

1. Ports `80` and `443` should be open to the word.

#### Setup basic auth to the site

1. Create a file with a name like `p.DOMAIN_location` in `vhost` folder.
    
    Where:

    `p` subdomain of site
    
    `DOMAIN` leave it like this. It will be subsituted during CI/CD
    
    `_location` leave it like this it is needed for [nginx-proxy](https://github.com/nginx-proxy/nginx-proxy#per-virtual_host-location-configuration)

1. Setup variables in settings of your CI/CD.
    You need `BASE_DOMAIN`. Example: `john.com`

1. This is template if you want to use auth on `every` IP exept `allow`:

    ```
    satisfy  any;
    allow xx.xx.xx.xx/32; # home
    deny all;
    ```
1. This is template if you want to forbid access of `every` IP exept `allow`:

    ```
    allow xx.xx.xx.xx/32;  # home
    deny all;

    ```
1. This is template if you want to use auth on `every` IP:

    ```
    satisfy  any;
    deny all;
    ```